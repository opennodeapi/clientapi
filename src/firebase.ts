import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';

// Run on local with nestjs

// admin.initializeApp({
//   credential: admin.credential.cert({
//     projectId: 'development-9cfa5',
//     clientEmail: 'firebase-adminsdk-uiwyo@development-9cfa5.iam.gserviceaccount.com',
//     privateKey: '-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQDbWizip2UY1UYf\nNXG9/8JhZ/ceCDYixh2De06CqzOm+nCR149E2GzP4XfR7ZSzMbUVBB0qgKawisB/\nHUHyyO8ZW87H3Kloclu+6U53H8ObfALNr2UrAy18XXLAdKU5fwg5M/Eh2E6lrqgi\nn+xEO0NqI7jAtdESiQ6x9z1szfXEFGu6Wm0YOceqfjpQVGiskBxq6yuMiQiiJgeT\nuAabs8z+A23HgBtDsYeXOnpoXy9nSDD+GkvCL8jZYiZP0f24+o1S2xc9QttMk6Fo\ntF7FwTk2CJuuZxsfqfAl+gpTLnLM8xsibY8TgAqW5TGEfBJ52SNAYBcrnCDnKaf/\nYc23JJY9AgMBAAECggEAGJrImqLyvuKbc0A8MjcIX4M5L7P1mJh28oroD1WzXUlN\ny7otfqg1KmtM1K4s5h3PBVy5Bc25BOgHeCq4vUpHMaW/7+DDWu0q9+L1M30LZLs4\nGEjSlQzN8xuLVeWvq9+hNUNriqOYDNMm1n1wU+X+5Z68qFcsrMLVdGZ6jCQdK4F2\nl9wSI2rAKyq+M8dtZi+KvCDXsn7Sf8am0WSiyor+hFtlFSmXO2Ff5ZYWiInYrdwO\n6aYUB59WBUfo74460qLFvXydRPK70CwccwuL19Vkph1fhMLmu1MVby3cUdIt+DdS\n38b4glyBEfjcFiUkyZqWZY3GeO50RP9O/NbdxzrgCQKBgQD+whC6SUKPEBCqd8Q/\nDHypsQmz9E9AOYkf9N4jzpMposNbCwuDe1Gw0qTghvLzEhsg9zb0kylOUugHZW6d\nArxKxkk90mf2guRzxNO9LhGF7TvhPcjG8zm91xbMIHFFVyaDoo05lEYR1dRXRUN1\nZ9eDO6pS9jOcl65gywVegq2piQKBgQDca+yLWDYDt5Zjjsixc9ETi9CIB0vUoOId\nPT2z2i9tcgLGvU12jYtQ1gyU9Q8b03c6b3KMTzdQdmh+BWohESXg9pQ+Q/uO6Ak6\nm01dR9yJbzFCs5rxQb+PZGy8pYTZLBnzP6GkCigH8DGg6vKeoPjSwsGNrzds5tdl\nmcqEiAK+FQKBgGK8ZC9O5XT9DPiEopKtDEPYUaJlNbH2mqbACLhn/7FLW9uVVEkJ\nKogVm8FJjC+Y/Nqx9aKkxoWMrFDxhwVjYga5Avnq6lAoOtKDnmzbxf7aCzMo9v/k\nlKR3EGbczAnL1Sl/fqyqQt32ue148sQ2EtjR7xloQmiWgQcSqGoQy63BAoGAJhs6\nX7S2UdoxU7DhnVwKbziiqoGoaHAP50IpIqi7vk4RkM41UCOUTDtVHYNoQ0VBvYZ6\n5SrkwzJVSkQhXuoaBFRcuI8ENGaFAe7LZy7/P3tLpt5qEP9u4JlZ+xsaZnkrW1RK\n25Mt7IFn8wvrOeVWXwlwcNAz4s3duyDlVBg3cSkCgYBVv/32dfl/E15SHeQvVzHx\nRloDnk5iaJM16BSVqM8Ogc74s8BReXUv1WIwX4ZNjCA7kQFUJU3yUirgLDJE4/Ev\ndgQI4+qcw5yAcrCy6wMCM1RqO6IQI1nBNwOyQPorvVY52aw55lPKn7MrXzMjBTLv\n/zY7OGOXtEOsWSw/d981mg==\n-----END PRIVATE KEY-----\n'
//   }),
//   databaseURL: 'https://development-9cfa5.firebaseio.com',
//   storageBucket: 'development-9cfa5.appspot.com',
// });

// Deploy to cloud functions

admin.initializeApp(functions.config().firebase);

const db = admin.firestore();
const bucket = admin.storage().bucket();

export {
    db,
    bucket,
};
