import { Module, NestModule, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';

@Module({
  imports: [ ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {

}
